<?php

namespace RaiaDrogasil\Aula\Controller\Test;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

use RaiaDrogasil\Aula\Model\Aula;
use RaiaDrogasil\Aula\Model\AulaFactory;
use RaiaDrogasil\Aula\Model\ResourceModel\Aula as AulaResource;

class Index extends Action
{
    protected $aulaModel;
    protected $aulaFactory;
    protected $aulaResource;

    public function __construct(
        Context $context,
        Aula $aula,
        AulaFactory $aulaFactory,
        AulaResource $aulaResource
    ) {
        $this->aulaModel = $aula;
        $this->aulaFactory = $aulaFactory;
        $this->aulaResource = $aulaResource;
        parent::__construct($context);
    }

    public function execute()
    {
        echo "Exemplo 1: <br>";
        $aula = $this->aulaModel->load(1);
        var_dump($aula->getData());

        echo "Exemplo 2: <br>";
        $aulaModel = $this->aulaFactory->create();
        $aulaModel->load(1);
        var_dump($aulaModel->getdata());

        echo "Exemplo 3: <br>";
        $aulaModel = $this->aulaFactory->create();
        $this->aulaResource->load($aulaModel, 1);
        var_dump($aulaModel->getData());
    }
}
