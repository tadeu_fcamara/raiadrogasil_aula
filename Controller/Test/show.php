<?php

namespace RaiaDrogasil\Aula\Controller\Test;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

use RaiaDrogasil\Aula\Model\AulaFactory;
use RaiaDrogasil\Aula\Model\ResourceModel\Aula as AulaResource;

class Show extends Action
{
    protected $aulaFactory;
    protected $aulaResource;

    public function __construct(
        Context $context,
        AulaFactory $aulaFactory,
        AulaResource $aulaResource
    ) {
        $this->aulaFactory = $aulaFactory;
        $this->aulaResource = $aulaResource;
        parent::__construct($context);
    }

    public function execute()
    {
        $aulaModel = $this->aulaFactory->create();
        $this->aulaResource->load($aulaModel, 1);
        var_dump($aulaModel->getData());
    }
}
