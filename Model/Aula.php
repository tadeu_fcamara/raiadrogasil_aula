<?php

namespace RaiaDrogasil\Aula\Model;

use Magento\Framework\Model\AbstractModel;
use RaiaDrogasil\Aula\Model\ResourceModel\Aula as AulaResource;

class Aula extends AbstractModel
{
    public function _construct()
    {
        $this->_init(AulaResource::class);
    }
}
