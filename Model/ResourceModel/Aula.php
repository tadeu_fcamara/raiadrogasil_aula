<?php

namespace RaiaDrogasil\Aula\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Aula extends AbstractDb
{
    public function _construct()
    {
        $this->_init("raiadrogasil_aula_entity", "id");
    }
}